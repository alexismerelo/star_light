use std::io;

fn main() {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let start = input_line.trim_end().to_string();

    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let target = input_line.trim_end().to_string();

    let mut current_state = start
        .chars()
        .map(|c| if c == '1' { true } else { false })
        .collect::<Vec<bool>>();

    let target = target
        .chars()
        .map(|c| if c == '1' { true } else { false })
        .collect::<Vec<bool>>();

    resolve(&mut current_state, &target);
}

fn resolve(current_state: &mut Vec<bool>, target: &Vec<bool>) {
    let last_value = current_state.len() - 1;

    let mut moves = 0;

    for index in 0..=last_value {
        if current_state[index] == target[index] {
            continue;
        }

        revers_value(current_state, index, last_value, &mut moves)
    }

    println!("{}", moves);
}

fn revers_value(current_state: &mut Vec<bool>, index: usize, last_value: usize, moves: &mut i32) {
    if index < last_value && !current_state[index + 1] {
        revers_value(current_state, index + 1, last_value, moves)
    }

    let is_rest_off = is_remaining_off(&current_state, index + 2, last_value);
    if !is_rest_off {
        turn_off_remaining(current_state, index + 2, last_value, moves);
    }

    current_state[index] = !current_state[index];
    *moves += 1;
}

fn turn_off_remaining(current_state: &mut Vec<bool>, index: usize, last_value: usize, moves: &mut i32) {
    for current_index in index..=last_value {

        match (current_state.get(current_index), current_state.get(current_index + 1)) {
            // if current_index == last_index
            (Some(v), None) if *v => {
                revers_value(current_state, current_index, last_value, moves)
            }
            // if i == true and i == true 
            (Some(v), Some(v_next)) if *v && *v_next=> {
                // set i to false
                revers_value(current_state, current_index, last_value, moves)
            }
            // if i + 1 is false
            (Some(v), Some(v_next)) if *v && !*v_next=> {
                // set i + 1 to true
                revers_value(current_state, current_index + 1, last_value, moves);
                // and set i to false
                revers_value(current_state, current_index, last_value, moves);
                // then set i + 1 to false
                revers_value(current_state, current_index + 1, last_value, moves);
            }
            _ => {}
        }
    }
}

fn is_remaining_off(current_state: &Vec<bool>, index: usize, last_value: usize) -> bool {
    // this will only be trigger by n and n - 1
    if last_value < index {
        return true;
    }

    let split_state = &current_state[index..];

    match split_state.iter().find(|&&x| x == true) {
        Some(_) => false,
        None => true,
    }
}
